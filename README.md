# Gofë

![License: AGPL](https://img.shields.io/badge/license-AGPL-%233897f0)

Gofë is a front-end for Google Search. It currently supports textual search.

The official instance—run by the primary developers—is
https://gofe.app. You can find a list of third-party instances
[here](INSTANCES.md).

## Why?

Gofë prevents Google's IP- and browser-fingerprinting by acting as a
middleperson between you and Google Search. This gives you better privacy.

## Roadmap

When we began working on Gofë, we planned support for image-, video-, and
news-search in the future; we also planned support for Google's "Instant
Answers". However, we realized that scraping results from Google was not worth
our time--sure, it could be somewhat edifying, but it is hardly an estimable
project. We have thus shifted focus on building an actual, open-source [search
engine](https://codeberg.org/ar324/voyna), based on a tier-based system
proposed by Drew Devault in a blog
[post](https://drewdevault.com/2020/11/17/Better-than-DuckDuckGo.html): this
venture's timescale is, of course, far longer, but we feel it worth our time and
effort. For the time being, we believe Gofë's textual support adequate for our
own (and most people's, really) needs.

Needless to say, we will still operate Gofë on https://gofe.app. We just won't
be adding new features, that's all: we'll still be maintaining it and running
the primary instance. For that reason, if you can support our infrastructure
costs by donating to us, please (still) do! (Moreover, it will help us with the
actual search engine we are building.)

## Technical Details

The API that fetches data from Google Search is written in Go and can be found
in `api/`. The root folder contains the Go server that utilizes this API to
handle client requests. The front-end is written in TypeScript, using NextJS:
the relevant source code can be found in `public/`.

## Screenshots

|                          |                                |
|--------------------------|--------------------------------|
| ![](/assets/images/home.png)    | ![](/assets/images/results.png)      |
| ![](/assets/images/privacy.png) | ![](/assets/images/noresults.png)      |

## License

Gofë (A front-end for Google Search.)

Copyright © 2022 Ajay R, Tristan B

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with this program.  If not, see <https://www.gnu.org/licenses/>.
