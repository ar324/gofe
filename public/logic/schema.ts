import { AlexandriaSchema } from "@xplato/alexandria"
import { colors } from "data"

export const schema: AlexandriaSchema = {
	accent: {
		allow: colors.filter(
			color => color !== "accent"
		) as unknown as string[],
		default: "blue",
	},
	actionLayout: {
		allow: ["list", "grid"],
		default: "list",
	},
	openLinksInNewTab: {
		allow: [true, false],
		default: false,
	},
}
