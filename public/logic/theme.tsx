import { MoonIcon, SunIcon, BoltIcon } from "@heroicons/react/24/solid"

export type Theme = "light" | "dark" | "system"

export const cycleTheme = (theme: Theme) => {
	switch (theme) {
		case "light":
			return "dark"
		case "dark":
			return "system"
		case "system":
			return "light"
	}
}

export const themeIcons = {
	light: <SunIcon />,
	dark: <MoonIcon />,
	system: <BoltIcon />,
}
