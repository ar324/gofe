export const isInternalLink = (url: string) => {
  return url.startsWith("/") || url.startsWith("#")
}
