export const pickRandomItems = <T>(items: T[], n: number): T[] => {
	const pickedItems: T[] = []
	const copiedItems = [...items]

	for (let i = 0; i < n; i++) {
		const index = Math.floor(Math.random() * copiedItems.length)
		pickedItems.push(copiedItems[index])
		copiedItems.splice(index, 1)
	}

	return pickedItems
}
