import axios from "axios"

import config from "../../../config.json"

import { getSearchPageURL } from "logic"
import { handleRequestError, wrapResponse } from "./err"

export const getSearchResults = async (query: string, page: number) => {
	return axios
		.get(`${config.api_domain}${getSearchPageURL(query, page)}`)
		.then(res => res.data)
		.catch(err => {
			return handleRequestError("getSearchResults", err)
		})
		.then(data => {
			console.log(data)
			return wrapResponse(data)
		})
}
