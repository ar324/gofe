const isServer = typeof window === "undefined"

export const getBlob = (key: string): string | undefined => {
	if (isServer) return undefined

	let blob

	try {
		blob = localStorage.getItem(key) || undefined
	} catch (e) {
		// Unsupported
	}

	return blob || undefined
}

export const getSavedObject = <T>(key: string, fallback: T): T => {
	const blob = getBlob(key)

	if (!blob) return fallback

	return JSON.parse(blob)
}

export const saveObject = <T>(key: string, value: T): void => {
	if (isServer) return

	try {
		localStorage.setItem(key, JSON.stringify(value))
	} catch (e) {
		// Unsupported
	}
}
