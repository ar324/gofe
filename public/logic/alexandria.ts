import { createAlexandria } from "@xplato/alexandria"
import { schema } from "logic/schema"

interface Settings {
	accent: string
	openLinksInNewTab: boolean
	actionLayout: string
}

const Alexandria = createAlexandria<Settings>(schema)
export const AlexandriaProvider = Alexandria.Provider
export const useAlexandria = Alexandria.useConsumer
