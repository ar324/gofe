import { useRouter } from "next/router"
import { NextComponentType, NextPageContext } from "next"
import { ThemeProvider } from "next-themes"
import { Toaster } from "react-hot-toast"
import classNames from "classnames"

import { useMemory } from "hooks"

import { AlexandriaProvider } from "logic"

import CommonStructure from "structures/CommonStructure"
import RootStructure from "structures/RootStructure"
import Entity from "components/motion/Entity"

import { variants } from "data"

import type { AppProps } from "next/app"
import type { Children } from "types/utils"

import "styles/seam.css"
import "styles/main.scss"
import "assets/fonts/inter.css"
import { AnimatePresence } from "framer-motion"

interface Props extends AppProps {
	Component: NextComponentType<NextPageContext, any, any> & {
		// Components can declare a structure for themselves. The structure property
		// is static which means that it is not calculated during the rendering
		// of the component itself. This is one way to preserve state between
		// route changes.
		//
		// The default component structure is the RootStructure component.
		structure?: ({ children }: Children) => JSX.Element
	}
}

const App = ({ Component, pageProps }: Props) => {
	const { pathname } = useRouter()
	const memory = useMemory()

	const Structure = Component.structure || RootStructure

	const getRootClassName = () => {
		if (pathname === "/") {
			return "index"
		}

		return pathname.replace("/", "").replace("/", "-").toLowerCase()
	}

	return (
		<AlexandriaProvider>
			<ThemeProvider attribute="class" defaultTheme="system">
				<div id="root">
					<div className={classNames("page", getRootClassName())}>
						<CommonStructure>
							<Structure>
								<Component {...pageProps} />
							</Structure>
						</CommonStructure>

						<AnimatePresence>
							{memory.overlay && (
								<Entity
									className="root-overlay"
									variants={variants.fade.default}
									transition={{
										type: "tween",
										duration: 0.25,
									}}
								/>
							)}
						</AnimatePresence>
					</div>
				</div>

				<Toaster />
			</ThemeProvider>
		</AlexandriaProvider>
	)
}

export default App
