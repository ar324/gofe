import { CheckIcon } from "@heroicons/react/20/solid"

import Button from "components/buttons/Button"
import DynamicLink from "components/links/DynamicLink"
import HTMLTitle from "components/utility/HTMLTitle"
import Card from "components/panels/Card"

const summaryPoints = [
	"No personal info is collected.",
	"No analytics are used.",
	"No cookies are used.",
	"No search queries are stored.",
]

const Privacy = () => {
	return (
		<>
			<HTMLTitle>Privacy</HTMLTitle>

			<section className="wfull flex-c py-16">
				<div className="mw padded">
					<p className="caption">Last updated: 21 December 2022</p>
					<h1 className="mt-4">Privacy</h1>

					<div className="wfull mt-8">
						<Card>
							<h2 className="title">Summary</h2>
							<div className="wfull grid grid-2 md:grid-1 mt-6">
								{summaryPoints.map((point, i) => (
									<div key={point} className="grid-block">
										<div className="HStack">
											<div className="w-6 h-6 bg-green-500 flex-c radius-90 mr-2">
												<i className="icon size-4 text-white">
													<CheckIcon />
												</i>
											</div>
											<div className="HStack">
												<p className="m-0">{point}</p>
											</div>
										</div>
									</div>
								))}
							</div>
						</Card>

						<div className="wfull grid grid-2 md:grid-1 gap-8 mt-8">
							<Card>
								<div className="SBStack">
									<h2 className="title">Cookies</h2>
									<h2 className="title">🍪</h2>
								</div>
								<p className="text">
									No cookies are used on this website. To
									remember your preferences, we use a feature
									in web browsers called{" "}
									<code>localStorage</code>. This allows you
									to customize your experience; this data is{" "}
									<span className="fw-600">not</span> sent to
									our server.
								</p>
							</Card>
							<Card>
								<div className="SBStack">
									<h2 className="title">Our Server</h2>
									<h2 className="title">💻</h2>
								</div>
								<p className="text">
									<DynamicLink href="https://gofe.app">
										gofe.app
									</DynamicLink>{" "}
									is self-hosted on a VPS server actively
									maintained by the two founders to the
									project. No information is collected apart
									from completely anonymous logs.
								</p>
							</Card>
							<Card>
								<div className="SBStack">
									<h2 className="title">Logs</h2>
									<h2 className="title">📉</h2>
								</div>
								<p className="text">
									We use logs to help us identify when
									something has gone wrong. They contain the
									following information:
								</p>
								<ul className="list mt-4 mb-0">
									<li>
										<p className="text m-0 lh-1-6">
											The date and time of requests
										</p>
									</li>
									<li>
										<p className="text m-0 lh-1-6">
											The endpoint that was hit (but{" "}
											<span className="fw-600">
												not your search query
											</span>
											)
										</p>
									</li>
									<li>
										<p className="text m-0 lh-1-6">
											The HTTP status code from the
											response
										</p>
									</li>
									<li>
										<p className="text m-0 lh-1-6">
											The total time it took to fulfill
											the request, in microseconds
										</p>
									</li>
								</ul>
							</Card>
							<Card>
								<div className="SBStack">
									<h2 className="title">Contact us</h2>
									<h2 className="title">☎️</h2>
								</div>
								<p className="text">
									If you have any questions about the
									information we collect, our project, or
									something else, feel free to reach out. If
									you're reporting a bug, please{" "}
									<DynamicLink href="https://codeberg.org/ar324/gofe/issues/new">
										open an issue
									</DynamicLink>{" "}
									instead.
								</p>
								<p className="text">
									Please contact us at{" "}
									<DynamicLink href="mailto:gofe@infinium.earth?subject=Hey Gofers">
										gofe@infinium.earth
									</DynamicLink>
								</p>
							</Card>
						</div>

						<div className="wfull flex-c">
							<Button href="/legal/privacy" className="mt-8">
								View the legal version
							</Button>
						</div>
					</div>
				</div>
			</section>
		</>
	)
}

export default Privacy
