import { useRouter } from "next/router"

import { useResults } from "hooks"

import TextResult from "components/search/results/TextResult"
import HTMLTitle from "components/utility/HTMLTitle"
import SearchError from "components/search/states/SearchError"
import RateLimited from "components/search/states/RateLimited"
import EmptyQuery from "components/search/states/EmptyQuery"
import ResultsLoading from "components/search/states/ResultsLoading"
import NoResults from "components/search/states/NoResults"
import RevalidatingDots from "components/search/fragments/RevalidatingDots"
import Button from "components/buttons/Button"

import { getSearchPageURL } from "logic"

const Search = () => {
	const { query: routerQuery, push } = useRouter()
	const query = routerQuery.q
	const page = routerQuery.p || 1

	const { results, loading, revalidating, err, dym, srf, rateLimited } =
		useResults(query, String(page))

	if (typeof query !== "string") {
		return null
	}

	if (err) {
		return <SearchError err={err} />
	}

	if (rateLimited) {
		return <RateLimited />
	}

	if (query.trim().length === 0) {
		return <EmptyQuery />
	}

	return (
		<>
			<HTMLTitle>{query || "Loading..."}</HTMLTitle>

			<section className="search-section">
				<div className="wfull mw-smaller h-8 mt-2">
					{revalidating ? (
						<RevalidatingDots />
					) : (
						(srf || dym) && (
							<div className="wfull mw-smaller mt-2 flex">
								{srf && (
									<p className="caption">
										Showing results for "
										<span className="fw-600">{srf}</span>"
									</p>
								)}
								{dym && (
									<p className="caption">
										Did you mean "
										<a
											href="#"
											onClick={() =>
												push(getSearchPageURL(dym))
											}
										>
											<span className="fw-600">
												{dym}
											</span>
										</a>
										?"
									</p>
								)}
							</div>
						)
					)}
				</div>

				{!loading && results && results.length === 0 && <NoResults />}

				{loading && (
					<div className="wfull mt-6">
						<ResultsLoading />
					</div>
				)}

				{results && results.length > 0 && (
					<div className="results mt-4">
						{!loading &&
							results &&
							results.length > 0 &&
							results.map((result, index) => (
								<TextResult
									key={result.URL}
									index={index}
									{...result}
								/>
							))}
					</div>
				)}

				{!loading && results && results.length > 0 && (
					<div className="wfull mw mt-12">
						<div className="wfull SBStack">
							<div className="ml-12 sm:ml-0">
								{(!page || (page && page === 1)) && (
									<Button
										href={getSearchPageURL(
											query,
											(Number(page) || 1) + 1
										)}
									>
										Next page
									</Button>
								)}
							</div>
						</div>
						{page > 1 && (
							<div className="wfull grid grid-3 gap-8 px-12 sm:px-0">
								<div className="grid-block">
									<Button
										href={getSearchPageURL(
											query,
											(Number(page) || 1) - 1
										)}
									>
										Previous
									</Button>
								</div>
								<div className="grid-block align-c">
									<p className="caption">Page {page}</p>
								</div>
								<div className="grid-block align-e">
									<Button
										href={getSearchPageURL(
											query,
											(Number(page) || 1) + 1
										)}
									>
										Next
									</Button>
								</div>
							</div>
						)}
					</div>
				)}
			</section>
		</>
	)
}

export default Search
