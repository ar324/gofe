import { ArrowSmallRightIcon } from "@heroicons/react/20/solid"

import HTMLTitle from "components/utility/HTMLTitle"
import Button from "components/buttons/Button"
import Card from "components/panels/Card"

const summaryPoints = [
	"Our website acts as an intermediary between Google and users by fetching search results on their behalf.",
	"We do not endorse or promote the information provided by Google, and we cannot verify its authenticity.",
	"We are not affiliated with Google and do not represent or speak on behalf of Google.",
	`Our website is provided on an "as is" and "as available" basis, without any representations or warranties.`,
	"We will not be liable for any damages arising from the use of our website.",
	"You agree to indemnify and hold us and our affiliates, officers, agents, and employees harmless from any claims or demands made by third parties.",
	"Any disputes arising from these Terms of Use or the use of our website will be resolved through binding arbitration in accordance with the Rules of Civil Procedure of the state of Utah in the United States of America.",
]

const Terms = () => {
	return (
		<>
			<HTMLTitle>Terms</HTMLTitle>

			<section className="wfull flex-c py-16">
				<div className="mw padded">
					<p className="caption">Last updated: 21 December 2022</p>
					<h1 className="mt-4">Terms</h1>

					<div className="wfull mt-8">
						<Card>
							<h2 className="title">Summary</h2>
							<div className="wfull grid grid-1 mt-6">
								{summaryPoints.map((point, i) => (
									<div key={point} className="grid-block">
										<div className="HStack align-s">
											<div className="mnw-6 w-6 h-6 bg-accent-500 flex-c radius-90 mr-2">
												<i className="icon size-4 text-white">
													<ArrowSmallRightIcon />
												</i>
											</div>
											<div className="HStack">
												<p className="m-0">{point}</p>
											</div>
										</div>
									</div>
								))}
							</div>
						</Card>

						<div className="wfull flex-c">
							<Button href="/legal/terms" className="mt-8">
								View the legal version
							</Button>
						</div>
					</div>
				</div>
			</section>
		</>
	)
}

export default Terms
