import Button from "components/buttons/Button"
import DynamicLink from "components/links/DynamicLink"
import HTMLTitle from "components/utility/HTMLTitle"

import { images } from "data"
import { pickRandomItems } from "logic"

const FourOhFour = () => {
	const randomImages = pickRandomItems(images, 4)

	return (
		<>
			<HTMLTitle>404</HTMLTitle>

			<section className="wfull flex-c py-16">
				<div className="mw padded">
					<p className="caption">Page not found</p>
					<h1 className="fs-29 mt-4">404</h1>

					<div className="wfull mt-8">
						<div className="mw-100">
							<p className="text lh-1-6">
								That's weird. The page you were looking for
								wasn't found on this website. If you believe
								this is a mistake, please{" "}
								<DynamicLink href="https://codeberg.org/ar324/gofe/issues/new">
									open an issue.
								</DynamicLink>
							</p>
						</div>

						<div className="wfull">
							<Button href="/" className="mt-8">
								Go back home
							</Button>
						</div>

						<div className="wfull grid grid-2 md:grid-1 gap-8 mt-16">
							{randomImages &&
								randomImages.map(image => (
									<div className="grid-block">
										<DynamicLink
											key={image}
											href="/search?q=dalle+openai"
											className="root-image-link"
										>
											<div className="root-image">
												<img
													src={image}
													alt="DALL•E generated, random image"
												/>
											</div>
										</DynamicLink>
									</div>
								))}
						</div>
					</div>
				</div>
			</section>
		</>
	)
}

export default FourOhFour
