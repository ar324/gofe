import { Html, Head, Main, NextScript } from "next/document"

import { config, root } from "data"

import configFile from "../../config.json"

const Document = () => {
	return (
		<Html lang="en">
			<Head>
				<link
					rel="icon"
					href={`data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>${config.faviconEmoji}</text></svg>`}
				/>
				<link
					rel='search'
					type='application/opensearchdescription+xml'
					title='Gofë Search'
					href={`${configFile.api_domain}/opensearch.xml`}
				/>
				<link rel="preconnect" href={root.cdn} />
				<meta name="description" content={config.description} />
			</Head>

			<body>
				<Main />
				<NextScript />
			</body>
		</Html>
	)
}

export default Document
