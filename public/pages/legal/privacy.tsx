import classNames from "classnames"

import HTMLTitle from "components/utility/HTMLTitle"
import Button from "components/buttons/Button"
import DynamicLink from "components/links/DynamicLink"

const Text = ({ className, children }: any) => (
	<p className={classNames("fs-2 lh-1-6 opacity-08", className)}>
		{children}
	</p>
)

const Privacy = () => {
	return (
		<>
			<HTMLTitle>Privacy</HTMLTitle>

			<section className="wfull flex-c py-16">
				<div className="mw">
					<Button href="/privacy" className="mb-8">
						Back
					</Button>

					<p className="caption">Last updated: 21 December 2022</p>
					<h1 className="mt-4">Privacy</h1>

					<Text>
						Gofë is an open-source front-end for Google Search. We
						act as a middleperson between you and Google Search: we
						fetch results on your behalf and deliver them to you
						privately. When you use Gofë, Google does not know that
						you made the request: it thinks that the request came
						from our servers.
					</Text>

					<div className="mt-8">
						<h2>Who we share your data with</h2>

						<Text>
							Only your search query is sent to Google, nothing
							more. We do not make requests to any other service.
						</Text>
					</div>

					<div className="mt-8">
						<h3>We do not...</h3>

						<ul>
							<li>
								<p className="m-0 mb-2">
									We <span className="fw-600">do not</span>{" "}
									store or collect any personally identifiable
									information.
								</p>
							</li>
							<li>
								<p className="m-0 mb-2">
									We <span className="fw-600">do not</span>{" "}
									collect your IP address.
								</p>
							</li>
							<li>
								<p className="m-0 mb-2">
									We <span className="fw-600">do not</span>{" "}
									attempt to uniquely identify your computer
									or browser via fingerprinting
									[soyerprinting] or any other method.
								</p>
							</li>
							<li>
								<p className="m-0 mb-2">
									We <span className="fw-600">do not</span>{" "}
									use analytics [soylytics] of any kind.
								</p>
							</li>
							<li>
								<p className="m-0 mb-2">
									We <span className="fw-600">do not</span>{" "}
									store cookies (although we do use
									localStorage, but this data is not sent to
									our servers).
								</p>
							</li>
						</ul>
					</div>

					<div className="mt-8">
						<h2>Information Contained in Our Logs</h2>

						<Text>
							We use logs only to identify when and if something
							goes wrong. These do not contain any personally
							identifiable information. Most notably (and most
							importantly):{" "}
							<span className="fw-600">
								our logs do not contain your search query
							</span>
						</Text>

						<ul className="mt-4">
							<li>
								<p className="m-0 mb-2">
									The date and time of requests
								</p>
							</li>
							<li>
								<p className="m-0 mb-2">
									The endpoint that was hit (
									<span className="fw-600">
										but not your search query
									</span>
									)
								</p>
							</li>
							<li>
								<p className="m-0 mb-2">
									The HTTP status code from the response
								</p>
							</li>
							<li>
								<p className="m-0 mb-2">
									The total time it took to fulfill the
									request, in microseconds
								</p>
							</li>
						</ul>
					</div>

					<div className="mt-8">
						<h2>Cookies & Local Storage</h2>
						<Text>We do not use cookies.</Text>
						<Text>
							This website uses localStorage to save your
							preferences in your browser. None of this
							information is sent to our server and can be deleted
							at any time by clearing your browser cache and site
							data.
						</Text>
					</div>

					<div className="mt-8">
						<h2>Contacting us</h2>
						<Text>
							If you have any questions about the information we
							collect, our project, or something else, feel free
							to reach out. If you're reporting a bug, please{" "}
							<DynamicLink href="https://codeberg.org/ar324/gofe/issues/new">
								open an issue
							</DynamicLink>{" "}
							instead.
						</Text>
						<Text>
							Please contact us at{" "}
							<DynamicLink href="mailto:gofe@infinium.earth?subject=Hey Gofers">
								gofe@infinium.earth
							</DynamicLink>
						</Text>
					</div>
				</div>
			</section>
		</>
	)
}

export default Privacy
