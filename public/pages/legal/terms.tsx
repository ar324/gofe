import classNames from "classnames"

import HTMLTitle from "components/utility/HTMLTitle"
import Button from "components/buttons/Button"
import DynamicLink from "components/links/DynamicLink"

const Text = ({ className, children }: any) => (
	<p className={classNames("fs-2 lh-1-6 opacity-08", className)}>
		{children}
	</p>
)

const Terms = () => {
	return (
		<>
			<HTMLTitle>Terms</HTMLTitle>

			<section className="wfull flex-c py-16">
				<div className="mw">
					<Button href="/terms" className="mb-8">
						Back
					</Button>

					<p className="caption">Last updated: 21 December 2022</p>
					<h1 className="mt-4">Terms of Use</h1>

					<p className="fs-4 fw-600 mt-8">Who we are</p>

					<Text>
						This website is operated by open-source volunteers and
						sponsored by{" "}
						<DynamicLink href="https://infinium.earth">
							Infinium LLC.
						</DynamicLink>
						. Contact information can be found at the bottom of this
						document.
					</Text>

					<div className="mt-8">
						<h2>1. Introduction</h2>

						<Text>
							Our website acts as an intermediary between Google
							and users by fetching search results on their
							behalf. By accessing or using our website, you agree
							to be bound by these Terms of Use and our Privacy
							Policy. If you do not agree to these terms, please
							do not use our website.
						</Text>
					</div>

					<div className="mt-8">
						<h2>2. Scope of Services</h2>

						<Text>
							Our website allows users to search for information
							on the internet anonymously by fetching search
							results from Google on their behalf. We do not
							endorse or promote the information provided by
							Google, and we cannot verify the authenticity or
							validity of the information. Our website merely acts
							as a conduit for users to access Google's search
							results.
						</Text>
					</div>

					<div className="mt-8">
						<h2>3. Disclaimer of Warranties</h2>

						<Text>
							Our website is provided on an "as is" and "as
							available" basis. We make no representations or
							warranties of any kind, express or implied, as to
							the operation of our website or the information,
							content, materials, or products included on our
							website. We do not warrant that our website will be
							uninterrupted or error-free, and we will not be
							liable for any interruptions or errors.
						</Text>
					</div>

					<div className="mt-8">
						<h2>4. Limitation of Liability</h2>

						<Text>
							In no event will we be liable for any damages of any
							kind arising from the use of our website, including
							but not limited to direct, indirect, incidental,
							punitive, and consequential damages.
						</Text>
					</div>

					<div className="mt-8">
						<h2>5. Indemnification</h2>

						<Text>
							You agree to indemnify and hold us and our
							affiliates, officers, agents, and employees harmless
							from any claim or demand, including reasonable
							attorneys' fees, made by any third party due to or
							arising out of your use of our website, your
							violation of these Terms of Use, or your violation
							of any rights of another.
						</Text>
					</div>

					<div className="mt-8">
						<h2>6. Changes to These Terms of Use</h2>

						<Text>
							We reserve the right to change these Terms of Use at
							any time. Any changes will be effective immediately
							upon posting on our website. Your continued use of
							our website after the posting of any changes
							constitutes your acceptance of those change
						</Text>
					</div>

					<div className="mt-8">
						<h2>7. Governing Law</h2>

						<Text>
							These Terms of Use and your use of our website shall
							be governed by and construed in accordance with the
							laws of the state of Utah, United States of America,
							without giving effect to any principles of conflicts
							of law.
						</Text>
					</div>

					<div className="mt-8">
						<h2>8. Dispute Resolution</h2>

						<Text>
							Any disputes arising from these Terms of Use or the
							use of our website will be resolved through binding
							arbitration in accordance with the Rules of Civil
							Procedure of the state of Utah in the United States
							of America. The arbitration will be conducted in the
							state of Utah, and the laws of the state of Utah
							will govern the arbitration proceedings.
						</Text>
					</div>

					<div className="mt-8">
						<h2>9. Miscellaneous</h2>

						<Text>
							These Terms of Use constitute the entire agreement
							between you and us with respect to our website. If
							any provision of these Terms of Use is found to be
							invalid or unenforceable, that provision shall be
							enforced to the maximum extent possible, and the
							remaining provisions shall remain in full force and
							effect.
						</Text>
					</div>

					<div className="mt-8">
						<h2>10. Contact Us</h2>

						<Text>
							If you have any questions about these Terms of Use,
							please contact us at{" "}
							<DynamicLink href="mailto:gofe@infinium.earth">
								gofe@infinium.earth
							</DynamicLink>
							.
						</Text>
					</div>
				</div>
			</section>
		</>
	)
}

export default Terms
