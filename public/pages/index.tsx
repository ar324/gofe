import NoStructure from "structures/NoStructutre"
import HTMLTitle from "components/utility/HTMLTitle"
import SearchBar from "components/search/SearchBar"
import Footer from "components/shell/Footer"
import InternalLink from "components/links/InternalLink"
import classNames from "classnames"

import Entity from "components/motion/Entity"

import { images, variants } from "data"
import { pickRandomItems } from "logic"

interface Props {
	images: string[]
}

const Home = ({ images }: Props) => {
	const getImageDelay = (index: number) => {
		const base = 0.2
		const multiplier = 0.15

		if (index === 1) {
			return base
		}

		if (index === 0 || index === 2) {
			return base + multiplier
		}

		return base
	}

	return (
		<>
			<HTMLTitle replaceTemplate>Gofë</HTMLTitle>

			<div className="panel padded">
				<Entity
					variants={variants.fade.in.up}
					transition={{
						type: "tween",
						duration: 0.4,
					}}
					className="wfull flex-c mb-16 relative z-9"
				>
					<h1 className="fs-21 lg:fs-18 md:fs-16 sm:fs-10 fw-500 ls--2 m-0 mb-4">
						Search privately with Gofë
					</h1>
					<div className="mw-100 flex-c text-c">
						<p className="lg:fs--1 opacity-07 m-0">
							Gofë is a private front-end for Google. It sends
							your query to Google and delivers the results back,
							privately, to you.
						</p>

						<SearchBar className="mt-8" />
					</div>
				</Entity>

				<div className="grid grid-3 sm:grid-1 gap-2 px-8">
					{images &&
						images.map((image, index) => (
							<div
								key={image}
								className={classNames(
									"grid-block",
									index > 0 && "sm:hide"
								)}
							>
								<Entity
									variants={variants.fade.default}
									transition={{
										type: "tween",
										duration: 0.6,
										delay: getImageDelay(index),
									}}
									className={classNames(
										"wfull flex",
										index === 0 && "align-e",
										index === 1 && "align-c",
										index === 2 && "align-s"
									)}
								>
									<InternalLink
										href="/search?q=DALLE+openai"
										className="root-image-link"
										name="DALL•E"
										title="DALL•E"
									>
										<div className="root-image">
											<img
												src={image}
												alt="DALL•E generated, random image"
												className="wfull"
											/>
										</div>
									</InternalLink>
								</Entity>
							</div>
						))}
				</div>
				<div className="wfull flex-c text-c mt-2">
					<Entity
						variants={variants.fade.default}
						transition={{
							type: "tween",
							duration: 0.4,
							delay: getImageDelay(0) + 0.1,
						}}
					>
						<InternalLink href="/search?q=DALLE+openai">
							<p className="caption">Synthesized by DALL•E</p>
						</InternalLink>
					</Entity>
				</div>
			</div>

			<Footer />
		</>
	)
}

Home.structure = NoStructure

export default Home

// We have to render the randomization on the server to
// avoid hydration issues.
export const getServerSideProps = async () => {
	return {
		props: {
			images: pickRandomItems(images, 3),
		},
	}
}
