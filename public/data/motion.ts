import type { Variant } from "framer-motion"

export interface Variants {
	hidden: Variant
	visible: Variant
	exit?: Variant
}

export const variants = {
	// General animations
	fade: {
		default: {
			hidden: { opacity: 0 },
			visible: { opacity: 1 },
		},
		in: {
			up: {
				hidden: { opacity: 0, y: 16 },
				visible: { opacity: 1, y: 0 },
			},
			right: {
				hidden: { opacity: 0, x: -16 },
				visible: { opacity: 1, x: 0 },
			},
		},
	},
	scale: {
		full: {
			hidden: { opacity: 0, scale: 0 },
			visible: { opacity: 1, scale: 1 },
		},
		half: {
			hidden: { opacity: 0, scale: 0.5 },
			visible: { opacity: 1, scale: 1 },
		},
		partial: {
			hidden: { opacity: 0, scale: 0.85 },
			visible: { opacity: 1, scale: 1 },
		},
		subtle: {
			hidden: { opacity: 0, scale: 0.95 },
			visible: { opacity: 1, scale: 1 },
		},
	},

	// Specicic animations
	page: {
		hidden: { opacity: 0, x: 32 },
		visible: { opacity: 1, x: 0 },
		exit: { opacity: 0, x: -32 },
	},

	// UI Component animations
	ui: {
		menu: {
			hidden: { opacity: 0, y: -6 },
			visible: { opacity: 1, y: 0 },
		},
	},
} as const

export const transition = {
	default: {
		type: "tween",
		duration: 0.5,
	},
	long: {
		type: "tween",
		duration: 1,
	},
	baseSpring: {
		type: "spring",
		bounce: 0.5,
		duration: 0.25,
	},
	page: {
		type: "tween",
		duration: 0.6,
		ease: [0.5, 0.08, 0.35, 0.96],
	},
}

export const spring = {
	smooth: { stiffness: 1000, damping: 100 },
	smoother: { stiffness: 500, damping: 100 },
	smoothest: { stiffness: 50, damping: 100 },
}

export const withDuration = (duration: number) => {
	return {
		...transition.default,
		duration,
	}
}

export const withDelay = (delay: number) => {
	return {
		...transition.default,
		delay,
	}
}
