export const colors = [
	"accent",
	"rose",
	"pink",
	"fuchsia",
	"purple",
	"violet",
	"indigo",
	"blue",
	"sky",
	"cyan",
	"teal",
	"emerald",
	"green",
	"lime",
	"yellow",
	"amber",
	"orange",
	"red",
] as const

export const shades = [
	"warmGray",
	"trueGray",
	"gray",
	"coolGray",
	"blueGray",
] as const
