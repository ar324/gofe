import { root } from 'data'

const _images = [
	"arrows-surfing",
	"astronaut",
	"cat",
	"doorway",
	"egyptian",
	"foggy",
	"forest",
	"london",
	"man",
	"mars-car",
	"oasis",
	"ocean-astronaut",
	"overgrown",
	"pirate",
	"plato",
	"prep-cat",
	"rocks",
	"ruins",
	"sand",
	"stripes",
	"the-cave",
	"the-thinker",
	"zeus",
]

export const images = _images.map(image => `${root.cdn}/dalle/${image}.png`)