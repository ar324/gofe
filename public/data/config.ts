export const config = {
	titleTemplate: " — Gofë",
	faviconEmoji: "🔍",
	description: "The anonymous front-end for Google Search",
} as const
