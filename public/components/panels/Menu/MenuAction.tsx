import { MouseEventHandler, useRef } from "react"
import { useRouter } from "next/router"
import classNames from "classnames"

import { generateMods } from "logic"

import type { Action } from "types/action"

interface Props extends Action {
	icon?: React.ReactNode
	click?: MouseEventHandler
	submenuOpen?: boolean
}

const MenuAction = ({
	href,
	click,
	label,
	icon,
	highlighted,
	submenuOpen,
}: Props) => {
	const ref = useRef<HTMLButtonElement>(null)
	const { push } = useRouter()

	const onClick: MouseEventHandler = ev => {
		if (typeof click === "function") {
			click(ev)
		}

		if (typeof href === "string") {
			push(href)
		}
	}

	return (
		<div className="action-wrap">
			<button
				ref={ref}
				onClick={onClick}
				className={classNames(
					"menu-action px-4 py-2-5",
					generateMods({ submenuOpen, highlighted })
				)}
				role="menuitem"
				tabIndex={0}
			>
				<div className="action-content">
					{icon && <i className="icon size-5">{icon}</i>}
					<span>{label}</span>
				</div>
			</button>
		</div>
	)
}

export default MenuAction
