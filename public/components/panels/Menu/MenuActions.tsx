import { MouseEventHandler } from "react"

import MenuAction from "components/panels/Menu/MenuAction"
import SubmenuItem from "components/panels/Menu/SubmenuItem"

import type { Action } from "types/action"

interface Props {
  actions: Action[]
  actionClickDoesCloseMenu?: boolean
  close: () => void
}

const MenuActions = ({ actions, actionClickDoesCloseMenu, close }: Props) => {
  return (
    <div className="w-100p actions" role="none">
      {actions.map((action, index) => {
        const onClick: MouseEventHandler = (ev) => {
          if (actionClickDoesCloseMenu && action.clickDoesCloseMenu !== false) {
            close()
          }

          if (typeof action.onClick === "function") {
            action.onClick(ev)
          }
        }

        if (action.label === "separator") {
          return <div key={action.id} className="separator"></div>
        }

        const actionProps = {
          index: index,
          click: onClick,
          ...action,
        }

        if (action.submenu) {
          return <SubmenuItem key={action.label} actionProps={actionProps} />
        }

        return <MenuAction key={action.label} {...actionProps} />
      })}
    </div>
  )
}

export default MenuActions
