import { ForwardedRef, forwardRef, useState } from "react"
import { AnimatePresence, useReducedMotion, motion } from "framer-motion"
import classNames from "classnames"

import MenuActions from "./MenuActions"
import Keyboard from "components/utility/Keyboard"

import { variants } from "data"

import type { Action } from "types/action"

export interface Props {
	isOpen: boolean
	close: () => void
	actions: Action[]

	origin?: string
	top?: number | string | boolean
	right?: number | string | boolean
	bottom?: number | string | boolean
	left?: number | string | boolean

	leaveDoesCloseMenu?: boolean
	actionClickDoesCloseMenu?: boolean

	[key: string]: any
}

const Menu = forwardRef(
	(
		{
			isOpen,
			close,
			actions,

			origin,
			top,
			right,
			bottom,
			left,

			leaveDoesCloseMenu,
			actionClickDoesCloseMenu,

			className,
			...props
		}: Props,
		ref: ForwardedRef<HTMLDivElement>
	) => {
		const [isAnimating, setIsAnimating] = useState(false)

		const shouldReduceMotion = useReducedMotion()

		const getPositionValue = (
			value: number | string | boolean | undefined
		): string => {
			if (typeof value === "undefined") {
				return "auto"
			}

			if (typeof value === "boolean") {
				return "0"
			}

			if (typeof value === "number") {
				return `${value}px`
			}

			return value
		}

		const onMouseLeave = () => {
			if (leaveDoesCloseMenu) {
				close()
			}
		}

		return (
			<>
				<AnimatePresence>
					{isOpen && (
						<motion.div
							ref={ref}
							variants={variants.scale.subtle}
							initial="hidden"
							animate="visible"
							exit="hidden"
							transition={
								shouldReduceMotion
									? {
											duration: 0,
									  }
									: {
											type: "spring",
											bounce: 0.5,
											duration: 0.5,
									  }
							}
							className={classNames("menu", className)}
							style={{
								transformOrigin: origin,
								top: getPositionValue(top),
								right: getPositionValue(right),
								bottom: getPositionValue(bottom),
								left: getPositionValue(left),
							}}
							role="menu"
							aria-orientation="vertical"
							aria-labelledby="menu-button"
							aria-busy={isAnimating}
							onAnimationStart={() => setIsAnimating(true)}
							onAnimationComplete={() => setIsAnimating(false)}
							onMouseLeave={onMouseLeave}
							{...props}
						>
							<MenuActions
								actions={actions}
								actionClickDoesCloseMenu={
									actionClickDoesCloseMenu
								}
								close={close}
							/>

							<Keyboard
								keys={["esc"]}
								onKeyPress={(key, ev) => {
									ev.preventDefault()

									if (key === "esc") {
										close()
										return
									}
								}}
								handleFocusableElements
								isExclusive
							/>
						</motion.div>
					)}
				</AnimatePresence>
			</>
		)
	}
)

Menu.defaultProps = {
	origin: "top left",
	top: "1rem",

	leaveDoesCloseMenu: false,
	actionClickDoesCloseMenu: true,
}

export default Menu
