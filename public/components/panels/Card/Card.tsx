import classNames from "classnames"

import { generateMods } from "logic"

import type { Children } from "types/utils"

interface Props extends Children {
	className?: string
}

const Card = ({ className, children }: Props) => {
	return (
		<>
			<div className={classNames("card", generateMods({}), className)}>
				{children}
			</div>
		</>
	)
}

export default Card
