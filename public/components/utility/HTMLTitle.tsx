import Head from "next/head"

import { config } from "data"

interface Props {
	children: React.ReactNode
	replaceTemplate?: boolean
}

const HTMLTitle = ({ children, replaceTemplate = false }: Props) => {
	let title = children?.toString() || "Loading..."
	if (!replaceTemplate) title += config.titleTemplate

	return (
		<Head>
			<title>{title}</title>
		</Head>
	)
}

export default HTMLTitle
