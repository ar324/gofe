import TextResultSkeleton from "./fragments/TextResultSkeleton"

const ResultsLoading = () => {
	return (
		<>
			<section className="search-section">
				<div className="results">
					{[1, 2, 3, 4, 5, 6, 7].map(e => (
						<TextResultSkeleton key={e} />
					))}
				</div>
			</section>
		</>
	)
}

export default ResultsLoading
