import Wrap from "components/search/states/fragments/Wrap"
import Title from "components/search/states/fragments/Title"
import Text from "components/search/states/fragments/Text"
import FatalErrorIllustration from "components/search/illustrations/FatalErrorIllustration"
import BaseIllustration from "components/search/states/fragments/BaseIllustration"

import { Children } from "types/utils"

interface Props extends Children {
	fatal?: boolean
	icon?: React.ReactNode
	title: string
	content: React.ReactNode
}

const Panel = ({ fatal, icon, title, content, children }: Props) => {
	return (
		<>
			<div className="panel">
				<Wrap>
					{fatal && (
						<BaseIllustration className="fatal mb-12">
							<FatalErrorIllustration />
						</BaseIllustration>
					)}
					{icon && !fatal && <div className="mb-4">{icon}</div>}
					<Title>{title}</Title>
					<Text>{content}</Text>
					{children}
				</Wrap>
			</div>
		</>
	)
}

export default Panel
