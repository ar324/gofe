import EmptyQueryIllustration from "components/search/illustrations/EmptyQueryIllustration"
import EmptyIllustration from "components/search/states/fragments/BaseIllustration"

const EmptyQuery = () => {
	return (
		<div className="panel pt-20">
			<EmptyIllustration className="p-8">
				<EmptyQueryIllustration />
			</EmptyIllustration>
			<h1 className="fs-15 ls--2 mt-6">Empty Query</h1>
			<div className="wfull mw-100 flex-c text-c">
				<p className="m-0">
					You may have accidentally searched for nothing. Try typing
					again in the search bar.
				</p>
				<div className="w-12 border-top-ui-3 my-4"></div>
				<p className="opacity-07 m-0">
					Pro-tip: Press "/" or "S" to highlight the search bar.
				</p>
			</div>
		</div>
	)
}

export default EmptyQuery
