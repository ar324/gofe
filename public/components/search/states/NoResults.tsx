import Button from "components/buttons/Button"
import EmptyIllustration from "components/search/states/fragments/BaseIllustration"

const NoResults = () => {
	return (
		<>
			<div className="wfull hfull flex-c pt-16">
				<EmptyIllustration />

				<div className="wfull mw-100 flex-c text-c mt-8">
					<h1 className="fs-12 ls--2 mt-0 mb-4">No results</h1>
					<p className="fs-2 lh-1-5 opacity-06 m-0">
						We couldn't find any results for your search. Try making
						your search less specific or try something
						tangential—you might get lucky!
					</p>
					<Button href="/" className="mt-6">
						Go home
					</Button>
				</div>
			</div>
		</>
	)
}

export default NoResults
