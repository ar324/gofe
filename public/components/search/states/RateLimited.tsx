import { toast } from "react-hot-toast"

import ErrorPanel from "components/search/states/ErrorPanel"

const RateLimited = () => {
	const onClick = () => {
		if (window && window.location) {
			window.location.reload()
		} else {
			toast.error("Unable to reload automatically.")
		}
	}

	return (
		<ErrorPanel
			title="Ain't that a kick in the head"
			content="Google has blocked our server from its search services. This should be temporary. If you still see this error in a few hours, please open an issue."
			action={{
				label: "Reload",
				onClick,
			}}
		/>
	)
}

export default RateLimited
