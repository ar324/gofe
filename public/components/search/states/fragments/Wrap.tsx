import { Children } from "types/utils"

const Wrap = ({ children }: Children) => (
	<div className="mw-100 flex-c text-c">{children}</div>
)

export default Wrap
