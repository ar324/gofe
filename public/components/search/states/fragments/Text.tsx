import { Children } from "types/utils"

const Text = ({ children }: Children) => <p className="m-0">{children}</p>

export default Text
