import { Children } from "types/utils"

const Title = ({ children }: Children) => (
	<h1 className="fs-14 ls--1 mb-3">{children}</h1>
)

export default Title
