import classNames from "classnames"

import ResultIllustration from "components/search/illustrations/ResultIllustration"

import { Children } from "types/utils"

interface Props extends Children {
	className?: string
}

const BaseIllustration = ({ className, children }: Props) => {
	return (
		<>
			<div className={classNames("base-illustration", className)}>
				{children || (
					<>
						<ResultIllustration />
						<ResultIllustration />
						<ResultIllustration />
					</>
				)}
				<div className="bg"></div>
			</div>
		</>
	)
}

export default BaseIllustration
