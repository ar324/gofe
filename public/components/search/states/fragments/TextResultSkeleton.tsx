const TextResultSkeleton = () => {
	return (
		<div className="text-result relative">
			<div className="wfull is-skeleton">
				<div className="mb-2">
					<div className="link-area">
						<div className="s-url skeleton"></div>
						<div className="s-description skeleton"></div>
					</div>
				</div>
				<div className="s-context one skeleton"></div>
				<div className="s-context two skeleton"></div>
			</div>
		</div>
	)
}

export default TextResultSkeleton
