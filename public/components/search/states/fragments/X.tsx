import { XMarkIcon } from "@heroicons/react/20/solid"

const X = () => (
	<div className="bg-red-500 radius-90 p-2 flex-c d-inline-flex mb-3">
		<i className="icon size-6 text-white">
			<XMarkIcon />
		</i>
	</div>
)

export default X
