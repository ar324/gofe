import { toast } from "react-hot-toast"

import ErrorPanel from "components/search/states/ErrorPanel"

import { APIError } from "logic"

const errorContent = {
	BadResponse: {
		title: "Something went wrong",
		description:
			"We encountered an error fetching your search results. If you keep getting this error, please open an issue.",
	},
	NoResponse: {
		title: "Server not responding",
		description:
			"The search server is not responding. If you continue to have this problem, please open an issue.",
	},
	BadRequest: {
		title: "Internal request error",
		description: (
			<span>
				There was an issue making a request to our search server. Please
				open an issue and include a screenshot of the console.
			</span>
		),
	},
}

interface Props {
	err: APIError
}

const SearchError = ({ err }: Props) => {
	const onClick = () => {
		if (window && window.location) {
			window.location.reload()
		} else {
			toast.error("Unable to reload automatically.")
		}
	}

	return (
		<ErrorPanel
			title={errorContent[err].title}
			content={errorContent[err].description}
			action={{
				label: "Reload",
				onClick,
			}}
		/>
	)
}

export default SearchError
