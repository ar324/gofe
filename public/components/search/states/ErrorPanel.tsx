import Panel from "components/search/states/Panel"
import Button from "components/buttons/Button"
import X from "components/search/states/fragments/X"

interface Props {
	title: string
	content: React.ReactNode
	action: {
		label: string
		onClick: () => void
	}
}

const ErrorPanel = ({ action, ...props }: Props) => {
	return (
		<Panel fatal icon={<X />} {...props}>
			<Button onClick={action.onClick} className="mt-6" color="red">
				{action.label}
			</Button>
		</Panel>
	)
}

export default ErrorPanel
