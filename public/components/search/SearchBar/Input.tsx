import { useEffect, useState } from "react"
import { useRouter } from "next/router"
import { AnimatePresence } from "framer-motion"
import { MagnifyingGlassIcon, XMarkIcon } from "@heroicons/react/20/solid"
import classNames from "classnames"

import Settings from "components/domain/Settings"
import Keyboard from "components/utility/Keyboard"
import Entity from "components/motion/Entity"
import Suggestions from "components/search/SearchBar/Suggestions"

import { Query, useDynamicPanel, useMemory } from "hooks"
import { generateMods } from "logic"
import { transition, variants } from "components/search/SearchBar/data"
import SettingsTrigger from "components/domain/SettingsTrigger"

interface Props {
	query: Query
	showHomeButton?: boolean
}

const Input = ({ query, showHomeButton }: Props) => {
	const { push } = useRouter()
	const memory = useMemory()
	const [isFocused, setIsFocused] = useState(false)

	const settingsPanel = useDynamicPanel()

	useEffect(() => {
		memory.set("overlay", settingsPanel.isOpen)
	}, [settingsPanel.isOpen])

	const onFocus = () => {
		setIsFocused(true)
	}

	const onBlur = () => {
		setIsFocused(false)
	}

	return (
		<>
			<div
				className={classNames(
					"input-wrap raise",
					generateMods({ showHomeButton })
				)}
			>
				{showHomeButton && (
					<button
						onClick={() => push("/")}
						className="searchbar-action raise"
						name="Home"
						title="Home"
					>
						<p className="fs-6 fw-300 m-0">ë</p>
					</button>
				)}

				<div className="input-control has-adornment raise">
					<button
						onClick={() => {
							if (query.isEmpty) {
								query.focus()
								query.select()
								return
							}

							query.submit()
						}}
						className="adornment"
						name="Search"
						title="Search"
					>
						<i className="icon size-4">
							<MagnifyingGlassIcon />
						</i>
					</button>
					<input
						ref={query.ref}
						type="text"
						placeholder="Search the web..."
						value={query.current}
						onChange={ev => {
							const value = ev.target.value
							query.setQuery(value)
							query.setConfirmed(value)
						}}
						onFocus={onFocus}
						onBlur={onBlur}
						onClick={() => {
							settingsPanel.close()
						}}
					/>
					{!query.isEmpty && (
						<button
							className="adornment right"
							onClick={query.clearQuery}
							name="Clear"
							title="Clear"
						>
							<i className="icon size-4">
								<XMarkIcon />
							</i>
						</button>
					)}
				</div>

				<SettingsTrigger settingsPanel={settingsPanel} />

				{showHomeButton && <div></div>}
				<Suggestions query={query} isFocused={isFocused} />
			</div>

			<AnimatePresence>
				{settingsPanel.isOpen && (
					<Entity
						key="settings"
						variants={variants}
						transition={transition}
						className="settings"
						ref={settingsPanel.ref}
					>
						<Settings settingsPanel={settingsPanel} />
					</Entity>
				)}
			</AnimatePresence>

			<Keyboard
				keys={["esc", "enter", ","]}
				onKeyPress={key => {
					if (key === "esc") {
						query.blur()
						settingsPanel.close()
					}

					if (key === "enter") {
						if (isFocused) {
							query.submit()
						}
					}

					if (key === ",") {
						settingsPanel.toggle()
					}
				}}
				handleFocusableElements
			/>
		</>
	)
}

export default Input
