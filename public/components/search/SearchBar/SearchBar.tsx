import { useQuery } from "hooks"
import classNames from "classnames"

import Input from "./Input"
import Keyboard from "components/utility/Keyboard"

interface Props {
	asNav?: boolean
	showHomeButton?: boolean
	className?: string
}

const SearchBar = ({ asNav, showHomeButton, className }: Props) => {
	const query = useQuery()

	return (
		<>
			<div
				className={classNames(
					asNav ? "searchbar" : "searchbar-island",
					className
				)}
			>
				<div className="wrapper">
					<Input query={query} showHomeButton={showHomeButton} />
				</div>
			</div>

			<div className="searchbar-fader"></div>

			<Keyboard
				keys={["s", "/", "."]}
				onKeyPress={(key, ev) => {
					ev.preventDefault()

					if (key === "s" || key === "/" || key === ".") {
						query.activate()
					}
				}}
			/>
		</>
	)
}

export default SearchBar
