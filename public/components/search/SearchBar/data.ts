export const variants = {
	hidden: {
		opacity: 0,
		y: -4,
	},
	visible: {
		opacity: 1,
		y: 0,
	},
}

export const transition = {
	type: "tween",
	// bounce: 0.5,
	duration: 0.15,
	delay: 0,
}
