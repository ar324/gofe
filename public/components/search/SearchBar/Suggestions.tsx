import { useEffect, useRef, useState } from "react"
import { useRouter } from "next/router"
import { AnimatePresence } from "framer-motion"
import classNames from "classnames"

import { useAlexandria } from "logic"
import { Query, useMemory, useSuggestions } from "hooks"

import Entity from "components/motion/Entity"
import Keyboard from "components/utility/Keyboard"

import { getSearchPageURL } from "logic"
import { transition, variants } from "components/search/SearchBar/data"

interface Props {
	query: Query
	isFocused: boolean
}

const Suggestions = ({ query, isFocused }: Props) => {
	const { push } = useRouter()
	const memory = useMemory()
	const ref = useRef<HTMLDivElement>(null)
	const alexandria = useAlexandria()

	const [suggestions, status] = useSuggestions(query.confirmed)
	const [showSuggestions, setShowSuggestions] = useState(false)
	const [selected, setSelected] = useState(-1)

	const select = (index: number) => {
		setSelected(index)
		query.setQuery(suggestions[index])
	}

	useEffect(() => {
		if (ref.current) {
			if (
				(!query.isEmpty && isFocused) ||
				(ref.current && ref.current.contains(document.activeElement))
			) {
				setShowSuggestions(true)
			}
		}

		if (!isFocused || !ref.current) {
			setShowSuggestions(false)
		}
	}, [query, isFocused])

	useEffect(() => {
		if (!isFocused) {
			setSelected(-1)
		}
	}, [isFocused])

	useEffect(() => {
		memory.set("overlay", showSuggestions)
	}, [showSuggestions])

	if (!alexandria.ready) return null

	return (
		<>
			<div
				ref={ref}
				className={classNames(
					"suggestions-wrap",
					alexandria.actionLayout === "grid" ? "grid" : "list"
				)}
			>
				<AnimatePresence>
					{showSuggestions && (
						<Entity
							// ref={ref}
							variants={variants}
							transition={transition}
							className="suggestions-box"
						>
							{suggestions.map((suggestion, index) => (
								<button
									key={suggestion}
									onClick={() => {
										query.setQuery(suggestion)
										push(getSearchPageURL(suggestion, 1))
									}}
									onFocus={() => select(index)}
									className={classNames(
										"suggestion",
										selected === index && "selected"
									)}
									id={`suggestion-${index}`}
								>
									<span>{suggestion}</span>
								</button>
							))}

							{status === "loading" && (
								<div className="dot-flashing"></div>
							)}

							{suggestions.length === 0 && (
								<p className="fs--1 opacity-6 m-0 ml-1">
									{status === "error"
										? "Unable to fetch suggestions :/"
										: "No suggestions :/"}
								</p>
							)}

							<Keyboard
								keys={["up", "down"]}
								onKeyPress={(key, ev) => {
									ev.preventDefault()

									if (key === "up") {
										select(Math.max(selected - 1, 0))
									}

									if (key === "down") {
										select(
											Math.min(
												selected + 1,
												suggestions.length - 1
											)
										)
									}
								}}
								handleFocusableElements
							/>
						</Entity>
					)}
				</AnimatePresence>
			</div>
		</>
	)
}

export default Suggestions
