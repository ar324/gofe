import InternalLink from "components/links/InternalLink"
import { useEffect, useState } from "react"

interface Props {}

const RevalidatingDots = ({}: Props) => {
	const [takingALongTime, setTakingALongTime] = useState(false)
	const [takingAReallyLongTime, setTakingAReallyLongTime] = useState(false)

	useEffect(() => {
		const timeout = setTimeout(() => {
			setTakingALongTime(true)
		}, 7000)

		const timeout2 = setTimeout(() => {
			setTakingAReallyLongTime(true)
		}, 14000)

		return () => {
			clearTimeout(timeout)
			clearTimeout(timeout2)
		}
	}, [])

	return (
		<div className="wfull HStack px-4 h-8">
			<div className="dot-flashing"></div>
			{takingALongTime && (
				<div className="ml-8 HStack">
					<p className="opacity-07 m-0">
						This is taking longer than expected...
					</p>
					{takingAReallyLongTime && (
						<InternalLink
							href="#"
							onClick={() => window.location.reload()}
							className="ml-2"
						>
							Reload the page
						</InternalLink>
					)}
				</div>
			)}
		</div>
	)
}

export default RevalidatingDots
