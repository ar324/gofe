import { ExclamationCircleIcon } from "@heroicons/react/20/solid"

const FatalErrorIllustration = () => {
	return (
		<>
			<div className="fatal-error-illustration">
				<div className="circle">
					<i className="icon size-8">
						<ExclamationCircleIcon />
					</i>
				</div>

				<div className="content">
					<div className="bar"></div>
				</div>
			</div>
		</>
	)
}

export default FatalErrorIllustration
