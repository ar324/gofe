import { MagnifyingGlassIcon } from "@heroicons/react/20/solid"

const ResultIllustration = () => {
	return (
		<>
			<div className="result-illustration">
				<div className="circle">
					<i className="icon">
						<MagnifyingGlassIcon />
					</i>
				</div>

				<div className="content">
					<div className="bar"></div>
					<div className="bar"></div>
				</div>
			</div>
		</>
	)
}

export default ResultIllustration
