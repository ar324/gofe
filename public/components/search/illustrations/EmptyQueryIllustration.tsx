import { QuestionMarkCircleIcon } from '@heroicons/react/24/solid'

const EmptyQueryIllustration = () => {
	return (
		<>
			<div className="empty-query-illustration">
				<div className="circle">
					<i className="icon size-8">
						<QuestionMarkCircleIcon />
					</i>
				</div>

				<div className="content">
					<div className="bar"></div>
				</div>
			</div>
		</>
	)
}

export default EmptyQueryIllustration
