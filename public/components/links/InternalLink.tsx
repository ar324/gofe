import Link from "next/link"

interface Props extends React.AnchorHTMLAttributes<HTMLAnchorElement> {
	href: string
	name?: string
	title?: string
}

const InternalLink = ({ href, children, ...props }: Props) => {
	return (
		<Link href={href} {...props}>
			{children}
		</Link>
	)
}

export default InternalLink
