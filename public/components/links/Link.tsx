import classNames from "classnames"
import { MouseEventHandler } from "react"

import type { Children } from "types/utils"

interface Props extends Children {
	href?: string
	onClick?: MouseEventHandler
	className?: string
}

const Link = ({ href, onClick: _onClick, className, children }: Props) => {
	const onClick: MouseEventHandler = ev => {
		ev.preventDefault()

		if (_onClick) {
			_onClick(ev)
		}
	}

	return (
		<>
			<a
				href={href || "javascript:void(0)"}
				className={classNames("link", className)}
				onClick={onClick}
			>
				{children}
			</a>
		</>
	)
}

export default Link
