import ExternalLink from "components/links/ExternalLink"
import InternalLink from "components/links/InternalLink"

import { isInternalLink } from "logic"

interface Props extends React.AnchorHTMLAttributes<HTMLAnchorElement> {
	href: string
}

const DynamicLink = ({ href, children, ...props }: Props) => {
	if (isInternalLink(href)) {
		return (
			<InternalLink href={href} {...props}>
				{children}
			</InternalLink>
		)
	}

	return (
		<ExternalLink href={href} {...props}>
			{children}
		</ExternalLink>
	)
}

export default DynamicLink
