interface Props extends React.AnchorHTMLAttributes<HTMLAnchorElement> {
	href: string
	openInNewTab?: boolean
}

const ExternalLink = ({
	href,
	openInNewTab,
	className,
	children,
	...props
}: Props) => {
	return (
		<a
			href={href}
			rel="nofollow noopener noreferrer"
			target={openInNewTab ? "_blank" : "_self"}
			className={className}
			{...props}
		>
			{children}
		</a>
	)
}

ExternalLink.defaultProps = {
	openInNewTab: true,
}

export default ExternalLink
