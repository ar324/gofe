import { HeartIcon } from "@heroicons/react/20/solid"
import ExternalLink from "./ExternalLink"

const SponsorLink = () => {
	return (
		<ExternalLink href="https://infinium.earth" className="link">
			<p className="flex align-c justify-c flex-row lh-1 fs--3 m-0">
				Backed with{" "}
				<i className="icon size-4 mx-1">
					<span className="text-red-500">
						<HeartIcon />
					</span>
				</i>{" "}
				by Infinium
			</p>
		</ExternalLink>
	)
}

export default SponsorLink
