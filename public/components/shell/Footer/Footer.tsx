import DynamicLink from "components/links/DynamicLink"
import SponsorLink from "components/links/SponsorLink"
import Entity from "components/motion/Entity"
import { variants } from "data"

const links = [
	{
		label: "Privacy",
		href: "/privacy",
	},
	{
		label: "Terms",
		href: "/terms",
	},
	{
		label: "Source code",
		href: "https://codeberg.org/ar324/gofe",
	},
	{
		label: "Report a bug",
		href: "https://codeberg.org/ar324/gofe/issues",
	},
	{
		label: "Request a feature",
		href: "https://codeberg.org/ar324/gofe/issues/new",
	},
]

const Footer = () => {
	return (
		<footer>
			<Entity
				variants={variants.fade.default}
				transition={{
					type: "tween",
					duration: 0.4,
					delay: 0.4,
				}}
				className="wfull flex-c"
			>
				<div className="wrap">
					<div className="wfull flex-c">
						<div className="flex justify-s align-s flex-wrap flex-row gap-6 mb-4 md:flex-c md:mb-6">
							{links.map(link => (
								<DynamicLink
									key={link.label}
									href={link.href}
									className="footer-link"
								>
									<span>{link.label}</span>
								</DynamicLink>
							))}
						</div>
						<SponsorLink />
					</div>
				</div>
			</Entity>
		</footer>
	)
}

export default Footer
