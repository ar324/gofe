export interface CommonToggleProps {
	onChange?: (value: boolean) => void
	layout?: "horizontal" | "vertical"
	label?: string
}