import { useCallback, useState } from "react"
import classNames from "classnames"

import { CommonToggleProps } from "components/form/types"
import { icons } from "data"

interface Props extends CommonToggleProps {
	defaultChecked?: boolean
}

const Checkbox = ({ label, layout, defaultChecked, onChange }: Props) => {
	const [checked, setChecked] = useState(defaultChecked || false)

	const toggle = useCallback(() => {
		setChecked(!checked)
		onChange && onChange(!checked)
	}, [checked])

	return (
		<>
			<div className={classNames("toggle-control", layout)}>
				<button
					onClick={toggle}
					className="checkbox"
					data-checked={checked}
				>
					{checked && <i className="icon size-5">{icons.check}</i>}
				</button>

				{label && (
					<div className="label-wrap" onClick={toggle}>
						<label>{label}</label>
					</div>
				)}
			</div>
		</>
	)
}

Checkbox.defaultProps = {
	layout: "horizontal",
}

export default Checkbox
