import { useCallback, useState } from "react"
import classNames from "classnames"

import { generateMods } from "logic"

import { CommonToggleProps } from "components/form/types"

interface Props extends CommonToggleProps {
	defaultOn?: boolean
}

const Switch = ({ defaultOn, onChange, label, layout }: Props) => {
	const [on, setOn] = useState(defaultOn || false)

	const toggle = useCallback(() => {
		setOn(!on)
		onChange && onChange(!on)
	}, [on])

	return (
		<>
			<div className={classNames("toggle-control", layout)}>
				<button
					onClick={toggle}
					className={classNames("switch", generateMods({ on }))}
				>
					<div className="bg"></div>
				</button>

				{label && (
					<div className="label-wrap" onClick={toggle}>
						<label>{label}</label>
					</div>
				)}
			</div>
		</>
	)
}

Switch.defaultProps = {
	layout: "horizontal",
}

export default Switch
