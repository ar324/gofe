import { MouseEventHandler } from "react"
import { useRouter } from "next/router"
import classNames from "classnames"

import { generateMods } from "logic"
import { colors } from "data"

interface Props extends React.HTMLAttributes<HTMLButtonElement> {
	href?: string
	altClass?: string
	size?: "sm" | "default" | "lg"
	color?: typeof colors[number]
}

const Button = ({
	href,
	altClass,
	size,
	color,
	onClick: _onClick,
	className,
	children,
	...props
}: Props) => {
	const { push } = useRouter()

	const onClick: MouseEventHandler<HTMLButtonElement> = ev => {
		if (href) push(href)
		if (_onClick) _onClick(ev)
	}

	return (
		<button
			className={classNames(
				altClass ?? "button",
				generateMods({ size, color }),
				className
			)}
			onClick={onClick}
			{...props}
		>
			{children}
		</button>
	)
}

Button.defaultProps = {
	size: "default",
	color: "accent",
}

export default Button
