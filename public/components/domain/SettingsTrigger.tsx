import { AnimatePresence } from "framer-motion"
import { XMarkIcon } from "@heroicons/react/20/solid"

import Entity from "components/motion/Entity"

import { DynamicPanel } from "hooks"

const transition = {
	type: "tween",
	duration: 0.1,
}

interface Props {
	settingsPanel: DynamicPanel<Element>
}

const SettingsTrigger = ({ settingsPanel }: Props) => {
	return (
		<>
			<button
				className="searchbar-action raise"
				onClick={settingsPanel.toggle}
				name="Settings"
				title="Settings"
			>
				<AnimatePresence mode="wait">
					{settingsPanel.isOpen ? (
						<Entity
							variants={{
								hidden: { y: -2, opacity: 0.2 },
								visible: { y: 0, opacity: 1 },
							}}
							transition={transition}
							key="x"
						>
							<i className="icon size-4">
								<XMarkIcon />
							</i>
						</Entity>
					) : (
						<Entity
							variants={{
								hidden: { y: 2, opacity: 0.2 },
								visible: { y: 0, opacity: 1 },
							}}
							transition={transition}
							key="cog"
						>
							<i className="icon size-4">
								<svg
									xmlns="http://www.w3.org/2000/svg"
									width="16"
									height="16"
									fill="currentColor"
									className="bi bi-magic"
									viewBox="0 0 16 16"
								>
									<path d="M9.5 2.672a.5.5 0 1 0 1 0V.843a.5.5 0 0 0-1 0v1.829Zm4.5.035A.5.5 0 0 0 13.293 2L12 3.293a.5.5 0 1 0 .707.707L14 2.707ZM7.293 4A.5.5 0 1 0 8 3.293L6.707 2A.5.5 0 0 0 6 2.707L7.293 4Zm-.621 2.5a.5.5 0 1 0 0-1H4.843a.5.5 0 1 0 0 1h1.829Zm8.485 0a.5.5 0 1 0 0-1h-1.829a.5.5 0 0 0 0 1h1.829ZM13.293 10A.5.5 0 1 0 14 9.293L12.707 8a.5.5 0 1 0-.707.707L13.293 10ZM9.5 11.157a.5.5 0 0 0 1 0V9.328a.5.5 0 0 0-1 0v1.829Zm1.854-5.097a.5.5 0 0 0 0-.706l-.708-.708a.5.5 0 0 0-.707 0L8.646 5.94a.5.5 0 0 0 0 .707l.708.708a.5.5 0 0 0 .707 0l1.293-1.293Zm-3 3a.5.5 0 0 0 0-.706l-.708-.708a.5.5 0 0 0-.707 0L.646 13.94a.5.5 0 0 0 0 .707l.708.708a.5.5 0 0 0 .707 0L8.354 9.06Z" />
								</svg>
							</i>
						</Entity>
					)}
				</AnimatePresence>
			</button>
		</>
	)
}

export default SettingsTrigger
