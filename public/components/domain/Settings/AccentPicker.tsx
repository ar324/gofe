import classNames from "classnames"

import { useAlexandria } from "logic"

import { colors } from "data"

interface Props {}

const AccentPicker = ({}: Props) => {
	const alexandria = useAlexandria()

	return (
		<div className="flex wfull">
			<p className="mini-text">Accent color</p>
			<div className="accent-picker mt-2">
				{colors
					.filter(color => {
						return (
							color !== "accent" &&
							color !== "indigo" &&
							color !== "violet" &&
							color !== "yellow" &&
							color !== "fuchsia" &&
							color !== "emerald" &&
							color !== "rose"
						)
					})
					.map(color => (
						<button
							key={color}
							className={classNames(
								"accent-button",
								`bg-${color}-500`,
								alexandria.accent === color && "selected"
							)}
							onClick={() => {
								alexandria.set("accent", color)
							}}
						>
							<div className="dot"></div>
						</button>
					))}
			</div>
		</div>
	)
}

export default AccentPicker
