import classNames from "classnames"

import { useAlexandria } from "logic"

import Switch from "components/form/Switch"

import { Settings } from "types/alexandria"

interface Props {
	id: keyof Settings
	label: string
	caption: string
	className?: string
}

const ToggleRow = ({ id, label, caption, className }: Props) => {
	const alexandria = useAlexandria()

	return (
		<>
			<div className={classNames("row wfull", className)}>
				<div className="flex">
					<p className="mini-text">{label}</p>
					<div className="HStack mt-2">
						<Switch
							defaultOn={Boolean(alexandria[id])}
							onChange={() => alexandria.toggle(id)}
							label={caption}
							layout="horizontal"
						/>
					</div>
				</div>
			</div>
		</>
	)
}

export default ToggleRow
