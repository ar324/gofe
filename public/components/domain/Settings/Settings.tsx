import { useTheme } from "next-themes"
import {
	CursorArrowRaysIcon,
	MoonIcon,
	SunIcon,
} from "@heroicons/react/24/outline"
import { XMarkIcon } from "@heroicons/react/20/solid"

import { useAlexandria } from "logic"
import { DynamicPanel } from "hooks"

import ToggleRow from "components/domain/Settings/ToggleRow"
import SelectRow from "components/domain/Settings/SelectRow"
import AccentPicker from "components/domain/Settings/AccentPicker"

import { capitalize } from "logic"

interface Props {
	settingsPanel: DynamicPanel<Element>
}

const Settings = ({ settingsPanel }: Props) => {
	const alexandria = useAlexandria()
	const { theme, setTheme, systemTheme } = useTheme()

	return (
		<>
			<div className="wfull flex">
				{/* <div className="wfull px-6 flex-sb hide sm:show">
					<button
						className="bg-transparent w-auto h-auto p-0 absolute top-8 right-8"
						onClick={settingsPanel.close}
					>
						<i className="icon">
							<XMarkIcon />
						</i>
					</button>
				</div> */}

				<SelectRow
					label="Theme"
					buttonLabel="Change theme"
					caption={`Theme is ${
						theme === "system" ? "automatic" : theme
					}`}
					options={[
						{
							label: `Automatic (${capitalize(
								systemTheme || "light"
							)})`,
							value: "system",
							icon: <CursorArrowRaysIcon />,
							highlighted: theme === "system",
						},
						{
							id: "separator-1",
							label: "separator",
							value: "separator",
						},
						{
							label: "Light",
							value: "light",
							icon: <SunIcon />,
							highlighted: theme === "light",
						},
						{
							label: "Dark",
							value: "dark",
							icon: <MoonIcon />,
							highlighted: theme === "dark",
						},
					]}
					onClick={option => setTheme(option?.value || "system")}
				/>

				<div className="divider"></div>

				<div className="row">
					<AccentPicker />
				</div>

				<div className="divider"></div>

				<ToggleRow
					id="openLinksInNewTab"
					label="Open links in new tab"
					caption={`Links open in
							${alexandria.openLinksInNewTab ? "a new tab" : "the current tab"}`}
				/>

				<div className="divider"></div>

				<SelectRow
					label="Suggestions layout"
					buttonLabel="Change layout"
					caption={`Viewing as ${alexandria.actionLayout}`}
					options={[
						{
							label: "List",
							value: "list",
							highlighted: alexandria.actionLayout === "list",
						},
						{
							label: "Grid",
							value: "grid",
							highlighted: alexandria.actionLayout === "grid",
						},
					]}
					onClick={option =>
						alexandria.set("actionLayout", option.value)
					}
				/>
			</div>
		</>
	)
}

export default Settings
