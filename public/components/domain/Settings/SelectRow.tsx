import classNames from "classnames"

import { useDynamicPanel } from "hooks"

import Button from "components/buttons/Button"
import Menu from "components/panels/Menu"

import { generateMods } from "logic"
import { Action } from "types/action"

interface Props {
	label: string
	buttonLabel: string
	options: Option[]
	caption: string
	onClick: (option: Option) => void
	className?: string
}

interface Option extends Action {
	value: string
}

const SelectRow = ({
	label,
	buttonLabel,
	options,
	caption,
	onClick,
	className,
}: Props) => {
	const { ref, isOpen, toggle, close } = useDynamicPanel()

	return (
		<>
			<div className={classNames("overlay", isOpen && "show")}></div>

			<div
				className={classNames(
					"row wfull",
					generateMods({ isOpen }),
					className
				)}
			>
				<div className="flex">
					<p className="mini-text">{label}</p>
					<div className="HStack mt-2">
						<div className="relative">
							<Button
								className={classNames("normal mr-2")}
								onClick={toggle}
								color="accent"
							>
								<span>{buttonLabel}</span>
							</Button>
							<Menu
								ref={ref as any}
								isOpen={isOpen}
								close={close}
								actions={options.map(option => ({
									onClick: () => {
										onClick(option)
										close()
									},
									...option,
								}))}
								top={40}
							/>
						</div>
						<p className="caption">{caption}</p>
					</div>
				</div>
			</div>
		</>
	)
}

export default SelectRow
