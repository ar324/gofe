import { useEffect, useState } from "react"

import { usePrevious } from "hooks"

import { getSearchResults } from "logic"
import { APIError } from "logic"
import { Result } from "types/search"

type QueryType = string | string[] | undefined

export const useResults = (query: QueryType, page: QueryType) => {
	const [results, setResults] = useState<Result[] | undefined>(undefined)
	const [err, setErr] = useState<APIError | false>(false)
	const [dym, setDym] = useState("")
	const [srf, setSrf] = useState("")
	const [rateLimited, setRateLimited] = useState(false)

	const previousQuery = usePrevious(query)
	const previousPage = usePrevious(page)

	// When the primary request is loading
	const [loading, setLoading] = useState(false)

	// Any revalidation done after the initial request
	// (this happens when a user enters a new search query)
	const [revalidating, setRevalidating] = useState(false)

	useEffect(() => {
		if (typeof query !== "string") {
			return
		}

		if (typeof results === "undefined") {
			setLoading(true)
		} else {
			setRevalidating(true)
		}

		if (query === previousQuery && page === previousPage) {
			setLoading(false)
			setRevalidating(false)
			return
		}

		getSearchResults(query, Number(page) || 1).then(data => {
			if (data.type === "success" && data.data) {
				if (Array.isArray(data.data.Links)) {
					setResults(data.data.Links)
				} else {
					setResults([])
				}

				setRateLimited(data.data.RateLimited)
				setDym(data.data?.DYM)
				setSrf(data.data?.SRF)
			} else {
				setResults([])
				setErr(data.errorType)
			}

			setLoading(false)
			setRevalidating(false)
		})
	}, [query, page])

	return { results, loading, revalidating, err, dym, srf, rateLimited }
}
