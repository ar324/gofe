import { useEffect } from "react"

export const useGlobalEvent = (listener: () => void) => {
	const onClick = () => {
		listener()
	}

	useEffect(() => {
		window.addEventListener("click", onClick)
		return () => window.removeEventListener("click", onClick)
	}, [])
}
