import { useEffect, useState } from "react"

import { getSuggestions } from "logic"
import { APIResponse, SuggestionResponse } from "types/api"

export const useSuggestions = (query: string): [string[], string] => {
	const [suggestions, setSuggestions] = useState<string[]>([])
	const [status, setStatus] = useState("idle")

	useEffect(() => {
		getSuggestions(query).then((data: APIResponse<SuggestionResponse>) => {
			if (data.type === "error") {
				setSuggestions([])
				setStatus("error")
				return []
			}

			if (data.type === "empty") {
				setSuggestions([])
				setStatus("noresults")
				return []
			}

			if (
				data.type === "success" &&
				data.data &&
				data.data.length === 2
			) {
				setSuggestions(data.data[1])
				setStatus("success")
				return data
			}

			setSuggestions([])
			return []
		})
	}, [query])

	return [suggestions, status]
}
