// A very simple, NON-API wrapper for input
// query state. Basically just compiles some
// common functions.

import { useEffect, useRef, useState } from "react"
import { useRouter } from "next/router"

import { getSearchPageURL } from "logic"

export interface Query {
	ref: React.RefObject<HTMLInputElement>

	// The current query; identical to the value of
	// the input
	current: string
	setQuery: (query: string) => void

	// The confirmed query; the query that suggestions
	// use. This is the same as current, unless the
	// selection has been changed, in which case the
	// current input is changed but the confirmed
	// query remains unset until the user initiates
	// the search.
	confirmed: string
	setConfirmed: (query: string) => void

	isEmpty: boolean
	select: () => void
	focus: () => void
	blur: () => void
	activate: () => void
	clearQuery: () => void
	submit: () => void
}

export const useQuery = (): Query => {
	const { query: nextQuery, push } = useRouter()
	const ref = useRef<HTMLInputElement>(null)

	const defaultQuery = typeof nextQuery.q === "string" ? nextQuery.q : ""

	const [query, setQuery] = useState(defaultQuery)
	const [confirmed, setConfirmed] = useState(defaultQuery)

	useEffect(() => {
		if (typeof nextQuery.q === "string") {
			setQuery(nextQuery.q)
		}
	}, [nextQuery])

	const isEmpty = query.trim() === ""

	const select = () => ref.current?.select()
	const focus = () => ref.current?.focus()
	const blur = () => ref.current?.blur()
	const activate = () => {
		select()
		focus()
	}
	const clearQuery = () => {
		setQuery("")
		focus()
	}
	const submit = () => {
		if (isEmpty) {
			return
		}

		blur()
		push(getSearchPageURL(query, 1))
	}

	return {
		ref,

		current: query,
		setQuery,

		confirmed,
		setConfirmed,

		isEmpty,

		select,
		focus,
		blur,
		activate,
		clearQuery,
		submit,
	}
}
