// An in-memory cache for arbitrary runtime data

import create from "zustand"

export interface Memory {
	overlay: boolean
}

export interface MemoryState {
	memory: Memory
	setMemory: (memory: Memory) => void
}

const useSessionStore = create<MemoryState>(set => ({
	memory: {
		overlay: false,
	},
	setMemory: (memory: Memory) =>
		set({
			memory,
		}),
}))

export const useMemory = () => {
	const memory = useSessionStore(state => state.memory)
	const setMemory = useSessionStore(state => state.setMemory)

	const set = <Key extends keyof Memory>(key: Key, value: Memory[Key]) => {
		setMemory({
			...memory,
			[key]: value,
		})
	}

	return {
		...memory,
    set,
	}
}
