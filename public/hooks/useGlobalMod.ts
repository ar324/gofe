import { useEffect } from "react"
import classNames from "classnames"

export const useGlobalMod = (
	mods: string | string[],
	querySelector: string = "html"
) => {
	useEffect(() => {
		const html = document.querySelector(querySelector)

		if (html) {
			html.classList.add(classNames(mods))
		}

		return () => {
			if (html) {
				html.classList.remove(classNames(mods))
			}
		}
	}, [mods])
}
