import SearchBar from "components/search/SearchBar"
import Footer from "components/shell/Footer"

import type { Children } from "types/utils"

interface Props extends Children {}

const RootStructure = ({ children }: Props) => {
	return (
		<div className="root-structure">
			<SearchBar asNav showHomeButton />
			{children}
			<Footer />
		</div>
	)
}

export default RootStructure
