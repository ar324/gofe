import type { Children } from "types/utils"

const NoStructure = ({ children }: Children) => {
	return <>{children}</>
}

export default NoStructure
