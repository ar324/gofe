import { useAlexandria } from "logic"
import { useGlobalMod } from "hooks"

import type { Children } from "types/utils"

interface Props extends Children {}

const CommonStructure = ({ children }: Props) => {
	const alexandria = useAlexandria()
	useGlobalMod(alexandria.accent as string)

	return <div className="common-structure">{children}</div>
}

export default CommonStructure
