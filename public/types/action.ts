import { MouseEventHandler } from "react"

export interface Action {
  id?: string // used as key if label === "separator"
  label: string // also used as key
  href?: string
  icon?: React.ReactNode
  active?: boolean
  highlighted?: boolean
  onClick?: MouseEventHandler
  submenu?: Action[]
  clickDoesCloseMenu?: boolean
}
