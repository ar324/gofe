import { APIError } from "logic"

export interface APIResponse<T> {
	type: "error" | "success" | "empty"
	errorType: APIError | false
	data: T
}

export type SuggestionResponse = [string, string[]]