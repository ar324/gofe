export interface Settings {
	accent: string;
	openLinksInNewTab: boolean;
	actionLayout: string;
}