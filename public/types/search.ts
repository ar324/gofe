export interface Result {
	URL: string
	Desc: string
	Context: string
}
