package api

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"strings"

	"golang.org/x/net/html"
)

// SearchConfig holds information pertaining to a search session.
type SearchConfig struct {
	Region string
	// SafeSearch, . . . to come
}

// SearchParameters holds information pertaining to a search query. Note that
// Page is 0-indexed.
type SearchParameters struct {
	Query string
	Page  int
}

// Valid checks if a SearchParameters is valid.
func (sp SearchParameters) Valid() bool {
	if sp.Page < 1 {
		return false
	}
	return true
}

// getQueryURL contructs the query URL for a given SearchParameters.
func (sc *SearchConfig) getQueryURL(qp SearchParameters) *url.URL {
	if sc.Region == "" {
		sc.Region = DefaultRegion // not a pointer receiver—don't fret
	}
	query := url.QueryEscape(qp.Query)
	u, err := url.Parse(fmt.Sprintf(qurl, query, (qp.Page-1)*10, sc.Region))
	if err != nil {
		panic(err) // Yeah, I'm certain.
	}
	return u
}

// Search queries Google Search returns a Result.
func (sc SearchConfig) Search(sp SearchParameters) (Result, error) {
	var rs Result

	if !sp.Valid() {
		return rs, errors.New("invalid SearchParameters")
	}

	if strings.TrimSpace(sp.Query) == "" {
		return rs, nil
	}

	u := sc.getQueryURL(sp)

	r := &http.Request{
		URL:    u,
		Header: make(http.Header),
	}

	client := timeoutClient()
	resp, err := client.Do(r)
	if err != nil {
		return rs, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		if isRateLimitCode(resp.StatusCode) {
			rs.RateLimited = true
			return rs, nil
		}
		return rs, fmt.Errorf("http.Get returned non-200 status code: %v", resp.StatusCode)
	}

	n, err := html.Parse(resp.Body)
	if err != nil {
		return rs, err
	}

	rs = findURLs(n)
	rs.sanitize()
	rs.DYM, rs.SRF = findExtra(n)
	return rs, nil
}
