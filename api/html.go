package api

import (
	"strings"

	"golang.org/x/net/html"
)

// getContent gets the textual content of an HTML node. It goes through the node's
// subtrees as well.
func getContent(n *html.Node) string {
	var b strings.Builder
	var gc func(n *html.Node)
	gc = func(n *html.Node) {
		if n == nil {
			return
		}
		if n.Type == html.TextNode {
			b.WriteString(n.Data)
			return
		}
		// depth-first search
		for n = n.FirstChild; n != nil; n = n.NextSibling {
			gc(n)
		}
	}
	gc(n)
	return b.String()
}

// findURLs finds URLs in a Google results page given its root HTML node, and
// returns a Result.
func findURLs(n *html.Node) Result {
	rs := Result{}
	var f func(*html.Node, bool)
	// main ensures we're only considering links inside <div id="main">
	f = func(n *html.Node, main bool) {
		if main && n.Type == html.ElementNode && n.Data == "a" {
			fc := n.FirstChild
			if fc != nil {
				fc = fc.FirstChild
			} else {
				return
			}
			if fc != nil {
				fc = fc.FirstChild
			} else {
				return
			}
			var desc string
			if fc != nil && fc.Data == "h3" {
				var link Link
				if fc.FirstChild != nil {
					desc = fc.FirstChild.FirstChild.Data
				}
				for _, v := range n.Attr {
					if v.Key == "href" {
						link.URL = v.Val
						link.Desc = strings.ToValidUTF8(desc, "")
						break
					}
				}
				// "context"
				if p := n.Parent; p.NextSibling != nil {
					p = p.NextSibling
					s := getContent(p)
					link.Context = strings.ToValidUTF8(s, "")
				}
				// my sincerest apologies
				if link.URL != "" {
					rs.Links = append(rs.Links, link)
				}
			}
		}
		if !main && n.Data == "div" {
			for _, v := range n.Attr {
				if v.Key == "id" && v.Val == "main" {
					main = true
				}
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c, main)
		}
	}
	f(n, false)
	return rs
}

// findExtra finds 'did you mean' or 'showing results for' in a Google results page.
func findExtra(n *html.Node) (dym string, srf string) {
	var f func(n *html.Node)
	f = func(n *html.Node) {
		if dym != "" && srf != "" {
			return
		}
		if n.Type == html.TextNode && strings.Contains(n.Data, "Did you mean:") {
			if n.NextSibling != nil {
				dym = getContent(n.NextSibling)
			}
		}
		if n.Type == html.TextNode && strings.Contains(n.Data, "Showing results for") {
			if n.NextSibling != nil {
				srf = getContent(n.NextSibling)
			}
		}
		if n.FirstChild != nil {
			f(n.FirstChild)
		}
		if n.NextSibling != nil {
			f(n.NextSibling)
		}
	}
	f(n)
	return dym, srf
}
