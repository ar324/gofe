package api

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
)

// Suggestions is a slice of strings to store suggestions in.
type Suggestions []string

// SugConfig holds information pertaining to a suggestion session.
type SugConfig struct {
	Region string
}

// getSuggestionsURL contructs the query URL for a given suggestion query.
func (sc SugConfig) getSuggestionsURL(query string) *url.URL {
	if sc.Region == "" {
		sc.Region = DefaultRegion // not a pointer receiver—don't fret
	}
	// Do we need SugParameters too? I do not think so.
	query = url.QueryEscape(query)
	u, err := url.Parse(fmt.Sprintf(surl, query, sc.Region))
	if err != nil {
		panic(err)
	}
	return u
}

// Suggest suggests queries based on `query` and returns a `Suggestions`. There might be
// an error, so do check for it before using the returned `Suggestions`.
func (sc SugConfig) Suggest(query string) (Suggestions, error) {
	if strings.TrimSpace(query) == "" {
		return Suggestions{}, nil
	}

	u := sc.getSuggestionsURL(query)
	req := &http.Request{
		URL:    u,
		Header: make(http.Header),
	}

	client := timeoutClient()
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("http.Get returned non-200 status code: %v", resp.StatusCode)
	}
	b, err := io.ReadAll(resp.Body)
	b = b[4:] // )]}' is needlessly prefixed to the response. any idea why?
	// response is ISO-8859-1 encoded
	// []byte -> []rune -> string -> []byte
	// there is of course a better way, I'm just too lazy at the moment
	rb := []byte(iso8859ToUTF8(b))

	var sr []interface{}
	err = json.Unmarshal(rb, &sr)
	if err != nil {
		return nil, fmt.Errorf("couldn't unmarshal: %v", err)
	}
	if len(sr) < 2 {
		return nil, fmt.Errorf("couldn't understand response")
	}
	r, ok := sr[1].([]interface{})
	if !ok {
		return nil, fmt.Errorf("couldn't understand response")
	}
	rs := make(Suggestions, len(r))
	for i, v := range r {
		switch vs := v.(type) {
		case string:
			rs[i] = vs
		default:
			return nil, fmt.Errorf("couldn't understand response")
		}
	}
	return rs, nil
}
