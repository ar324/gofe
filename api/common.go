package api

import (
	"net/http"
	"time"
)

// isRateLimitCode checks if sc is a response code having to do with being rate-
// limited.
func isRateLimitCode(sc int) bool {
	rl := map[int]bool{
		301: true,
		302: true,
		429: true,
	}
	return rl[sc]
}

// timeoutClient constructs an http.Client with a timeout of 20 seconds.
func timeoutClient() *http.Client {
	return &http.Client{
		Timeout: 20 * time.Second,
	}
}

// iso8859ToUTF8 converts an ISO-8859-1 encoded byte slice to a UTF-8 string.
func iso8859ToUTF8(b []byte) string {
	buf := make([]rune, len(b))
	for i, v := range b {
		buf[i] = rune(v)
	}
	return string(buf)
}
