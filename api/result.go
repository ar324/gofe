package api

// Result holds a complete search result.
type Result struct {
	Links []Link
	// RateLimited tells api's clients if Google has rate limited their requests.
	RateLimited bool
	// DYM holds the "Did you mean" suggestion, if returned by Google for a particular query.
	DYM string
	// SRF holds the "Showing results for" text, if returned by Google for a particular query.
	SRF string
}

// sanitize sanitizes all of a Result object's Links.
func (rs *Result) sanitize() {
	var links []Link
	for _, v := range rs.Links {
		v.sanitize()
		if v.URL == "" {
			continue
		}
		links = append(links, v)
	}
	rs.Links = links
}
