package api

import (
	"log"
	"net/url"
)

// Link represents a single search result.
type Link struct {
	URL string
	// Desc is typically the <title> of the webpage.
	Desc string
	// Context holds text that shows why the URL is pertinent to the search query.
	Context string
}

// sanitize removes Google's trackers from a URL.
func (l *Link) sanitize() {
	// l.URL is in the following form: /url?q= . . .
	u, err := url.Parse(l.URL)
	if err != nil {
		log.Printf("sanitize: could not parse %q; error: %v", l.URL, err)
		l.URL = ""
		return
	}

	// Google sometimes returns an actual link, for its own pages.
	if u.IsAbs() {
		u.RawQuery = ""
		l.URL = u.String()
		return
	}

	s, err := url.PathUnescape(u.Query().Get("q")) // 'q' houses the link
	if err != nil {
		log.Printf("sanitize: could not extract the link from %s; error: %v", u, err)
		l.URL = ""
		return
	}
	l.URL = s
}
