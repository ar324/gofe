package api

import ()

const (
	// qurl represents the URL format to query Google Search.
	qurl = "https://google.com/search?" +
		"&q=%s" + // query
		"&start=%d" + // page number
		"&gl=%s" // Region

	// surl represents the URL format to get suggestions from Google Search.
	surl = "https://www.google.com/complete/search?&client=firefox&xssi=t" +
		"&q=%s" +
		"&gl=%s"

	// DefaultRegion is a two-letter country code that denotes the default region to pass to Google Search for location-aware results.
	DefaultRegion = "US"
)
